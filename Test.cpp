/*

**********************************************
*************** FROM THE AUTHOR***************
**********************************************

Goal of this project is to calculate the RGB means of the Images
It will be beneficial to use this parameter in the Deep learning project
Also this project is my exercise to understand better parallelism, concurrency and SSE operations.

-------------------------------------

To compile the file, please use this command in the main catalog:
g++ -std=c++11 -pthread -o Test Test.cpp `pkg-config --libs --cflags opencv4` 
where:
-X - put the number of the images

Program was tested on the Ubuntu 18.04

Status of the code:
1. Prof of Concept - it works, but nothing more
2. Optimized - it works quite well, I tried to optimize the code, so It should work better
3. Project finished - it  is well optimized and it's not prone to errors. Errors and exceptions are properly handled

Current status: 1

Author: Hazel_Eyes

*/
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>
#include <sstream>
#include <thread>
#include <iostream>
#include <algorithm>
#include "Additional_functions.hpp"
#include <numeric>
#include <iterator>

#define ONE_THREAD 1
#define BLUE 0
#define GREEN 1
#define RED 2

int main(){

//decide, if you want to print Basic information about the system
#ifdef INFO
    std::cout << "\nBasic Information: \n";
    //std::cout << "Number of the images per thread: " << NUMBER_OF_THE_IMAGES / static_cast<int>(std::thread::hardware_concurrency())  << std::endl;    
    std::cout << "Number of threads, that avaible in the system: " << std::thread::hardware_concurrency() << std::endl;
    std::cout << "\n";
#endif    

    std::vector<std::string> v_ImagesPaths;
    //vector of the threads

    //opening a file:
    std::fstream v_File;
    std::string v_Line;

    std::vector<std::thread> v_Thread_Vector;
    std::vector<std::vector<unsigned int>> v_Results_of_threads;
    short v_ImPerThr;

    //Adding Images to the vector...
    v_File.open("Images_Names.txt");
    while (std::getline(v_File,v_Line)){
        v_ImagesPaths.push_back(v_Line);
    }
    v_File.close();

    //checking size of the v_ImagesPaths. If number of images is less than the number of threasd,
    //There will be only one image per thread
    if(std::thread::hardware_concurrency() > v_ImagesPaths.size()){
        v_Thread_Vector.resize(v_ImagesPaths.size());
        v_Results_of_threads.resize(v_ImagesPaths.size(),std::vector<unsigned int>(3,0));
        v_ImPerThr = ONE_THREAD;
    }
    else{
        v_Thread_Vector.resize(std::thread::hardware_concurrency());
        v_Results_of_threads.resize(std::thread::hardware_concurrency(),std::vector<unsigned int>(3,0));
        v_ImPerThr = v_ImagesPaths.size() / std::thread::hardware_concurrency();
    }
    

    #ifdef INFO
        std::cout << "Number of images in the vector: " << v_ImagesPaths.size() << std::endl; 
        std::cout <<"Number of threads per image: " << v_ImPerThr << std::endl;
    #endif
    

    for (size_t i = 0; i < v_Thread_Vector.size(); i++)
    {
        v_Thread_Vector[i] = std::thread(Processing_Images,v_ImagesPaths.begin() + v_ImPerThr * i,(v_ImagesPaths.begin() + i * v_ImPerThr) + v_ImPerThr ,std::ref(v_Results_of_threads[i]));
    }

    //joining threads;
    for (auto &th : v_Thread_Vector){
            th.join();       
    }

    //acumulting pixels
    for(auto it = v_Results_of_threads.begin();it != v_Results_of_threads.end();it++){
        v_Results_of_threads[0][BLUE] += it->at(BLUE);
        v_Results_of_threads[0][GREEN] += it->at(GREEN);
        v_Results_of_threads[0][RED] += it->at(RED);
    }
    //dividing
    auto v_BlueMean = static_cast<float>(v_Results_of_threads[0][BLUE] / v_Results_of_threads.size());
    auto v_GreenMean = static_cast<float>(v_Results_of_threads[0][GREEN] / v_Results_of_threads.size());
    auto v_RedMean = static_cast<float>(v_Results_of_threads[0][RED] / v_Results_of_threads.size());



    #ifdef INFO
    //showing results:
    std::cout << "Results of the averages: " << std::endl;
    for(int i = 0; i < v_Thread_Vector.size();i++){
        std::cout <<"Blue " << i << ": " << v_Results_of_threads[i][0] << std::endl;
        std::cout <<"Green " << i << ": " << v_Results_of_threads[i][1] << std::endl;
        std::cout <<"Red " << i << ": " << v_Results_of_threads[i][2] << std::endl;

    }
    #endif

    //Saving to the file
    std::ofstream f("RGB-mean.txt");
    f <<("Blue channels mean: ") << std::fixed << std::setprecision(2) << v_BlueMean << std::endl;
    f << ("Green channels mean: ") << std::fixed << std::setprecision(2) << v_GreenMean << std::endl;
    f << ("Red channels mean: ") << std::fixed << std::setprecision(2) << v_RedMean << std::endl;
    f.close();
    
    std::cout << "End of the program\n";
}
 