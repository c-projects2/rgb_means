#ifndef ADD_FUNC_HPP
#define ADD_FUNC_HPP


/*

**********************************************
*************** FROM THE AUTHOR***************
**********************************************

Goal of this project is to calculate the RGB means of the Images
It will be beneficial to use this parameter in the Deep learning project
Also this project is my exercise to understand better parallelism, concurrency and SSE operations.

-------------------------------------

Implementation of the function, that should process image, calculate its RGB mean and add to the  

Program was tested on the Ubuntu 18.04

Status of the code:
1. Prof of Concept - it works, but improvements can and WILL be done
2. Optimized - it works quite well, I tried to optimize the code, so It should work better
3. Project finished - it  is well optimized and it's not prone to errors. Errors and exceptions are properly handled

Current status: 1 is finished

Additional things to do next:


Author: Hazel_Eyes

*/

#include <vector>
#include <string>

int Alignment(int alignment, int pixelNumbers);
void Processing_Images(std::vector<std::string>::iterator it_begin, std::vector<std::string>::iterator it_end, std::vector<unsigned int> & pixel_mean);



#endif