
#include "Additional_functions.hpp"
#include <vector>
#include <iterator>
#include <string>
#include <iostream>
#include <thread>
#include <gtest/gtest-death-test.h>
#include <opencv4/opencv2/opencv.hpp>
#include <typeinfo>
#include <tuple>

#define BLUE 0
#define GREEN 1
#define RED 2
#define TOP_ALIGNMENT 0
#define LEFT_ALIGNMENT 0
#define BLACK_PIXEL 0
#define SSE_ALIGNMENT 32

int Alignment(int alignment, int pixelNumbers){
    if((pixelNumbers % alignment) == 0){
        return pixelNumbers;
    }
    else{
        return (alignment  - (pixelNumbers % alignment));    
    }   
}


void Processing_Images(std::vector<std::string>::iterator it_begin, std::vector<std::string>::iterator it_end, std::vector<unsigned int> & pixel_mean){

    std::vector<std::vector<unsigned int>> t_Results;
    //std::cout << std::this_thread::get_id() << std::endl; 
    for(std::vector<std::string>::iterator it = it_begin;it != it_end;it++){
        cv::Mat t_Image = cv::imread(*it);
        #ifdef INFO
            std::cout << "Columns of the image: " << t_Image.cols << std::endl;
            std::cout << "Rows of the image: " << t_Image.rows << std::endl;
            std::cout << "Path to the image: " << *it << std::endl;
        #endif
        //calculating pixels
        //int bottom_alignment = Alignment(32,t_Image.rows);
        int bottom_alignment = Alignment(32,t_Image.rows);
        int right_alignment = Alignment(32,t_Image.cols);
        cv::copyMakeBorder(t_Image,t_Image,0,bottom_alignment,0,right_alignment,0);
        #ifdef INFO
            std::cout << "Columns of the image after: " << t_Image.cols << std::endl;
            std::cout << "Rows of the image after: " << t_Image.rows << std::endl;
        #endif
        unsigned int numberOfPixels = t_Image.rows * t_Image.cols;
        unsigned int blue_mean = 0;
        unsigned int green_mean = 0;
        unsigned int red_mean = 0;
        //calucalting mean of the image
        //oldfashion way, using pathetic value by 
        for(int i = 0; i < t_Image.rows;i++){
            for(int j = 0; j < t_Image.cols;j++){
                unsigned char * p = t_Image.ptr(i,j);
                blue_mean += static_cast<unsigned int>(p[0]);
                green_mean += static_cast<unsigned int>(p[1]);
                red_mean += static_cast<unsigned int>(p[2]);
            }
        }

        numberOfPixels -= ((bottom_alignment * right_alignment) + right_alignment * t_Image.rows + bottom_alignment * t_Image.cols);
        blue_mean /= numberOfPixels; 
        green_mean /= numberOfPixels;
        red_mean /= numberOfPixels;

        std::vector<unsigned int>t_PixelAverage = {blue_mean,green_mean,red_mean};
        #ifdef INFO
            std::cout << "Blue value: " << blue_mean<< std::endl;
            std::cout << "Green value: " << green_mean<< std::endl;
            std::cout << "Red value: " << red_mean << std::endl;
        #endif

        t_Results.push_back(t_PixelAverage);
    }
    std::vector<std::vector<unsigned int>> v_result = {{0,0,0}};

    for(auto it = t_Results.begin();it != t_Results.end();it++){

        v_result[0][BLUE] += it->at(BLUE);
        v_result[0][GREEN] += it->at(GREEN);
        v_result[0][RED] += it->at(RED);
    }   

    v_result[0][BLUE] /= t_Results.size(); 
    v_result[0][GREEN] /= t_Results.size();
    v_result[0][RED] /= t_Results.size();

    pixel_mean = v_result[0];
}
