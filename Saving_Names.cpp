/*

**********************************************
*************** FROM THE AUTHOR***************
**********************************************

Goal of this project is to calculate the RGB means of the Images
It will be beneficial to use this parameter in the Deep learning project
Also this project is my exercise to understand better parallelism, concurrency and SSE operations.

-------------------------------------

To compile the file, please use this command in the main catalog:
g++ Saving_Names.cpp -lstdc++fs -std=c++17 -o Saving -DTEST
where:
-DTEST is optional, if you want to examine what is going on inside the program

Program was tested on the Ubuntu 18.04

Status of the code:
1. Prof of Concept - it works, but nothing more
2. Optimized - it works quite well, I tried to optimize the code, so It should work better
3. Project finished - it  is well optimized and it's not prone to errors. Errors and exceptions are properly handled

Current status: 1

Author: Hazel_Eyes

*/
#include <iostream>
#include <experimental/filesystem>
#include <string>
#include <stack>
#include <fstream>

#define Check 10

int main(){
    
    #ifdef INFO
        std::cout << "\nCurrent path, where the file is located: " << std::experimental::filesystem::current_path() << std::endl;
    #endif

    std::string v_Path = static_cast<std::string>(std::experimental::filesystem::current_path());
    v_Path += "/Images/";

    //Handler to the file
    std::ofstream v_File;
    std::stack<std::string> v_FileNames;
    const std::experimental::filesystem::path v_CurrentDirectory(v_Path);

    #ifdef INFO
        std::cout <<"Folder, where images should be saved: " <<v_CurrentDirectory << std::endl;
    #endif

    for(const auto &t_file : std::experimental::filesystem::directory_iterator(v_CurrentDirectory)){
        #ifdef INFO
            std::cout <<"Path added to the file: " << t_file.path() << std::endl;
        #endif
        v_FileNames.push(t_file.path());
    }
    
    try{
        v_File.open("Images_Names.txt");
    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
        return -1;
    }
    
    for (int i = 1;v_FileNames.empty() == false;i++)
    {
        v_File << (v_FileNames.top() + "\n");
        v_FileNames.pop();
        if ( (i % Check) == 0.0 && i != 1){
            std::cout << "[INFO] Saved Images: " << i <<"..."<< std::endl;
        }
    }
    v_File.close();
    #ifdef INFO
        std::cout << "Program has been finished!\n";
        std::cout << "All paths to the images are written in the Images_Names.txt.\n";
    #endif
    return 0;
}